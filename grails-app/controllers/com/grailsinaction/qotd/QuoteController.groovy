package com.grailsinaction.qotd

class QuoteController {
    static scaffold = true
    //  This is the default method called when using the controller, unless a new default action is set.
    // def index() { }
    static defaultAction = "home"
    def quoteService

    def random() {
        def pickOne = (1..9)
        def isLower = pickOne.get(4)
        def randomQuote = quoteService.getRandomQuote()
        [quote: randomQuote]
    }
}
